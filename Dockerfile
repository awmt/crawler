FROM node

# Create app directory
WORKDIR /usr/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY tsconfig*.json ./
COPY package*.json ./

# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

RUN npm install

# for typescript
RUN npm run build
RUN npm ci --quiet && npm run build

#RUN npm run build
COPY .env ./dist/
WORKDIR ./dist

EXPOSE 4000

#CMD npm start --live-reload --poll=3000
#CMD [ "node", "src/index.js" ]
#CMD node src/index.js
