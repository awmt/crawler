MIB34 AWMT - Gruppe C - 2021
_Auf Basis des simplecrawlers
https://github.com/simplecrawler/simplecrawler_


Wie wir dieses Projekt starten

Voraussetzungen: _docker_ und _docker-compose_     
- .env aus der .env.example erstellen
   - wichtig ist hier _API_AUTH_, hier muss der gleiche Code wie auch in der API genutzt werden
- `yarn docker:build` oder `npm run docker:build`

