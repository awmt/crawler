import CrawlerService from "./CrawlerService";
import { Message } from "discord.js";
// @ts-ignore
import fs from "fs";
// @ts-ignore
import archiver from "archiver";
// @ts-ignore
import path from "path";
// @ts-ignore
import rimraf from "rimraf";

/**
 * This class manages all accessible commands
 */
class CommandService {
  commands: string[];
  crawler: CrawlerService;
  actions: any;

  constructor(commands: string[], crawler: CrawlerService, message: Message) {
    this.commands = commands;
    this.crawler = crawler;

    //This attribute holds the commands functionality
    this.actions = {
      "!report": {
        command: "!report",
        action: () => {
          const modules = crawler.news;

          message.channel.send(
            "There are currently " + modules.length + " news articles registered!"
          );
          return;
        },
      },
      "!download": {
        command: "!download",
        action: async () => {
          const dirName: string = new Date(Date.now()).getTime().toString();
          const modules = crawler.news;
          let files: any[] = [];
          let zipName: string = "";

          if (modules.length < 1) {
            await message.channel.send("There are no new articles registered!");
            return;
          }

          fs.mkdirSync("./" + dirName);

          for (const key in modules) {
            const item = modules[key].attributes;
            const output = "./" + dirName + "/" + key.toString() + ".json";
            saveJson(item, output);
            files.push(fs.readFileSync(output));
          }

          files = await loadZip(path.join("./", dirName, "/"), "./");
          zipName = files[0];

          await message.channel.send("Your files:", { files: files });
          await rimraf.sync(path.join("./", dirName, "/"));
          await fs.unlinkSync(zipName);
        },
      },
    };

    if (this.commands[0].toLowerCase() === "!crawler") {
      this.crawlerExecute();
    }
  }

  //Calling an action
  crawlerExecute() {
    this.actions[this.commands[1]].action();
  }
}

export default CommandService;

/**
 * responsible for saving data
 * @param data
 * @param output
 */
const saveJson = (data: any, output: string) => {
  fs.writeFileSync(output, JSON.stringify(data));
};

/**
 * Responsible for loading all JSONs as a .zip
 * @param filedir
 * @param outputdir
 */
const loadZip = async (filedir: string, outputdir: string) => {
  const name: string = new Date(Date.now()).getTime().toString();
  const zipPath = path.join(outputdir, name);
  const output = fs.createWriteStream(zipPath + ".zip");
  const archive = await archiver("zip", {
    zlib: { level: 9 }, // Sets the compression level.
  });

  await output.on("close", function () {
    // console.log(archive.pointer() + ' total bytes');
    // console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  await archive.on("error", function (err: any) {
    throw err;
  });

  await archive.pipe(output);
  // archive.glob('*.json');

  // append files from a sub-directory and naming it `new-subdir` within the archive (see docs for more options):
  await archive.directory(filedir, false);
  await archive.finalize();

  return [zipPath + ".zip"];
};
