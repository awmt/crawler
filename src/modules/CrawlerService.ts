// @ts-ignore
import Crawler from "simplecrawler";
// @ts-ignore
import { QueueItem } from "simplecrawler/queue";
import NewsModule from "./NewsModule";
// @ts-ignore
import moment from "moment";

/**
 * This class represents the main crawler functionality
 */
class CrawlerService {
  crawler: Crawler;
  news: NewsModule[];
  regex: {
    start: RegExp;
    end: RegExp;
    index: RegExp;
  };

  constructor() {
    const today = new Date(Date.now());
    const year = moment(today).format("YYYY");
    const month = moment(today).format("MM");

    this.news = [];
    this.crawler = new Crawler(process.env.CRAWL_SITE + "/news/" + year + "/" + month + "/");
    // this.crawler = new Crawler("https://www.wiwo.de/");
    this.regex = {
      start: RegExp("^https:\\/\\/www.nabu.de\\/news\\/"),
      end: RegExp(".html$"),
      index: RegExp("index.html"),
    };

    this.properties();
    this.events(this);
  }

  //Here we set the crawlers properties
  private properties() {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    this.crawler.interval = 10000; // Ten seconds
    this.crawler.maxConcurrency = 3;
    this.crawler.maxDepth = 10;
    this.crawler.respectRobotsTxt = true;
    this.crawler.acceptCookies = true;
    this.crawler.ignoreInvalidSSL = true;
  }

  /**
   * The crawlers events are stored here
   * @param service
   */
  private events(service: CrawlerService) {
    /**
     * In this instance we only need a discoverycomplete
     * this event locates links, evaluates them and creates a NewsModule object for each link
     */
    // @ts-ignore
    this.crawler.on("discoverycomplete", function (queueItem: QueueItem, resources: string[]) {
      console.log("Discovery Complete!", resources.length);
      const today = new Date(Date.now());
      const year = moment(today).format("YYYY");
      const month = moment(today).format("MM");

      for (const key in resources) {
        const item = resources[key];
        if (testLink(item, service.regex, year, month)) {
          console.log("Valid Link discovered", key, item);
          if (!service.linkExists(item)) {
            console.log("!!!!!!!!!!!!!!!!!!!New Link Discoverd!!!!!!!!!!!!!!!!!!!", item);
            const newsModule = new NewsModule({ url: item });
            newsModule.start();
            service.news.push(newsModule);
          }
        }
      }
    });

    this.crawler.on("crawlstart", () => {
      console.log("CrawlerService crawler start");
    });
  }

  //Starting the crawler
  start() {
    this.crawler.start();
  }

  //Stopping the crawler
  stop() {
    this.crawler.stop();
  }

  /**
   * Does the given link already exists as a NewsModule?
   * @param link
   */
  private linkExists(link: string) {
    for (const key in this.news) {
      const item = this.news[key];
      if (item.name === link) {
        item.start();
        return true;
      }
    }

    return false;
  }
}

export default CrawlerService;

/**
 * Does the Link meet our specifications?
 * @param item
 * @param regex
 * @param year
 * @param month
 */
const testLink = (
  item: string,
  regex: { start: RegExp; end: RegExp; index: RegExp },
  year: string,
  month: string
) => {
  if (regex.start.test(item) && regex.end.test(item) && !regex.index.test(item)) {
    if (item.includes(year + "/" + month)) {
      return true;
    }
  }

  return false;
};
