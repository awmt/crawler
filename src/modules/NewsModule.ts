require("dotenv").config({ path: ".env" });

// @ts-ignore
import Crawler from "simplecrawler";
import INewsModuleProps from "../interfaces/INewsModuleProps";
// @ts-ignore
import { QueueItem } from "simplecrawler/queue";
import { IncomingMessage } from "http";
// @ts-ignore
import { parse } from "node-html-parser";
// @ts-ignore
import moment from "moment";
import ApiModule from "./ApiModule";

const apiUrl: string = process.env.API_URL || "";
const code: string = process.env.API_AUTH || "";
const apiModule = new ApiModule(apiUrl, code);
const momentFormat = "MM-DD-YYYY HH:mm:ss";

interface IAttributes {
  id?: string | null;
  h1?: string | null;
  h2?: string | null;
  image?: string | null;
  teaser?: any | null;
  url: string;
  date?: string | null;
  lastCrawled: string;
}

/**
 * This class is responsible for one news article only
 */
class NewsModule {
  crawler: Crawler;
  name: string;
  attributes: IAttributes;

  constructor(props: INewsModuleProps) {
    const { url } = props;
    this.name = url;
    this.attributes = {
      url: url,
      lastCrawled: moment(new Date(Date.now())).format(momentFormat),
    };
    this.post();
    this.crawler = new Crawler(url);
    this.properties();
    this.events(this);
  }

  //Here we set the crawlers properties
  private properties() {
    this.crawler.interval = 60 * 1000; // every minute
    this.crawler.maxConcurrency = 3;
    this.crawler.maxDepth = 1;
  }

  /**
   * The crawlers events are stored here
   * @param module
   */
  private events(module: NewsModule) {
    /**
     * Here we only need the fetchcomplete event
     * This event has the responseBuffer with the complete dom inside
     * from there we pick our attributes and store them inside this objects state
     */
    this.crawler.on(
      "fetchcomplete",
      // @ts-ignore
      function(queueItem: QueueItem, responseBuffer: string | Buffer, response: IncomingMessage) {
        const dom = responseBuffer.toString();
        const html = parse(dom);

        module.attributes.lastCrawled = moment(new Date(Date.now())).format(momentFormat);
        module.attributes.h1 = null;
        module.attributes.h2 = null;
        module.attributes.teaser = null;

        if (html.querySelector("h1")) {
          module.attributes.h1 = html.querySelector("h1").childNodes[0].rawText;
        }

        if (html.querySelector("h2")) {
          module.attributes.h2 = html.querySelector("h2").childNodes[0].rawText;
        }

        if (html.querySelector(".teaser-text p")) {
          module.attributes.teaser = html.querySelector(".teaser-text p").childNodes[0].rawText;
        }

        if (html.querySelector(".image img")) {
          // @ts-ignore
          const imgRawAttrs = html.querySelector(".image img").rawAttrs;
          const split1 = imgRawAttrs.split("src=\"");
          const split2 = split1[1].split("\"");

          module.attributes.image = process.env.CRAWL_SITE + split2[0];
        }

        const selectorB = html.querySelector("#container-inner p b");
        const selectorStrong = html.querySelector("#container-inner p strong");

        moment.locale('de');

        if (selectorStrong) {
          const textDate: string = selectorStrong.childNodes[0].rawText;

          module.attributes.date = moment(textDate, 'LL').toDate();
        } else if (selectorB) {
          const textDate: string = selectorB.childNodes[0].rawText;

          module.attributes.date = moment(textDate, 'LL').toDate();
        }

        const request = async () => {
          await module.put();

          module.stop();
        };

        request();
      },
    );
  }

  private async post() {
    const res = await apiModule.postNews(this.attributes);
    if (res.exists) {
      this.attributes.id = res.id;
      await this.put();
      return;
    }
    this.attributes.id = res.id;
  }

  private async put() {
    // console.log("try PUT", this.attributes);
    await apiModule.putNews(this.attributes);
  }

  //Starting the crawler
  start() {
    this.crawler.start();
  }

  //Stopping the crawler
  stop() {
    this.crawler.stop();
  }
}

export default NewsModule;
