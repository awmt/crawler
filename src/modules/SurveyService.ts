import fetch, { Response } from "node-fetch";
import ISurveyData, { IAnswer } from "../interfaces/ISurveyData";
import ApiModule from "./ApiModule";

require("dotenv").config({ path: ".env" });

interface IProps {
  fetchEndpoint: string;
  apiUrl: string;
}

class SurveyService {
  fetchEndpoint: string;
  apiUrl: string;
  apiModule: ApiModule;

  constructor(props: IProps) {
    const { fetchEndpoint, apiUrl } = props;

    this.fetchEndpoint = fetchEndpoint;
    this.apiUrl = apiUrl;
    this.apiModule = new ApiModule(this.apiUrl, process.env.API_AUTH || "");
    try {
      this.start()
          .then()
          .catch()
    } catch (e) {
      console.log(e);
    }
  }

  private start = async () => {
    setInterval(this.fetchSurvey, 1000);
  };

  private fetchSurvey = async () => {
    try {
      const res: Response = await fetch(this.fetchEndpoint);
      const data = await res.json();
      const result = data.results[0];
      const prismicSurvey = result.data.survey;
      let surveyData: ISurveyData = { answers: [], last_publication_date: "", question: "" };

      surveyData.last_publication_date = result.last_publication_date;
      surveyData.question = prismicSurvey.title.value[0].text;

      for (const key in prismicSurvey) {
        if (key.includes("choice")) {
          const answer: IAnswer = { content: prismicSurvey[key].value };
          surveyData.answers.push(answer);
        }
      }

      const surveyExists = !(await this.surveyExists(surveyData));
      if (surveyExists) {
        const newSurvey = await this.apiModule.post(
            { question: surveyData.question, last_publication_date: surveyData.last_publication_date },
            "api/survey"
        );

        if (!newSurvey.error) {
          for (const key in surveyData.answers) {
            const answer = surveyData.answers[key];
            await this.apiModule.post(
                { content: answer.content, survey: newSurvey.id },
                "api/answer"
            );
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  private surveyExists = async (item: ISurveyData) => {
    const savedSurveys = await this.apiModule.get("api/survey");
    for (const key in savedSurveys) {
      const survey = savedSurveys[key];
      if (survey.last_publication_date === item.last_publication_date) return true;
    }

    return false;
  };
}

export default SurveyService;
