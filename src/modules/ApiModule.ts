import fetch, { HeadersInit, Response } from "node-fetch";

class ApiModule {
  url: string;
  authorization: string;
  headers: HeadersInit;

  constructor(url: string, code: string) {
    this.url = url;
    this.authorization = code;
    this.headers = {
      "Content-Type": "application/json",
      Authorization: code,
    };
  }

  async getNews() {
    const endpoint: string = "api/news";

    return await this.request({ method: "GET", endpoint });
  }

  async postNews(data: any) {
    const existing = await this.doesExist(data.url);

    if (existing.exists) return existing;
    const endpoint: string = "api/news";

    return await this.request({ method: "POST", body: JSON.stringify(data), endpoint });
  }

  async putNews(data: any) {
    const endpoint: string = "api/news";

    return await this.request({ method: "PUT", body: JSON.stringify(data), endpoint });
  }

  async get(endpoint: string) {
    return await this.request({ method: "Get", endpoint });
  }

  async post(data: any, endpoint: string) {
    return await this.request({ method: "POST", body: JSON.stringify(data), endpoint });
  }

  async put(data: any, endpoint: string) {
    return await this.request({ method: "PUT", body: JSON.stringify(data), endpoint });
  }

  private async doesExist(url: string): Promise<{ exists: boolean; id?: string }> {
    const news = await this.getNews();
    for (let i = news.length - 1; i >= 0; i--) {
      const entry = news[i];
      if (entry.url === url) {
        return { exists: true, id: entry.id };
      }
    }

    return { exists: false };
  }

  private async request(params: { method: string; body?: any; endpoint: string }) {
    const { method, body, endpoint } = params;

    const res: Response = await fetch(this.url + endpoint, { method, body, headers: this.headers });
    return await res.json();
  }
}

export default ApiModule;
