interface ISurveyData {
  last_publication_date: string;
  question: string;
  answers: IAnswer[];
}

export interface IAnswer {
  content: string;
  surveyId?: string;
}

export default ISurveyData;
