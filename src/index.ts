import CommandService from "./modules/CommandService";

require("dotenv").config({path: ".env"});
// @ts-ignore
import Discord, {Guild, GuildMember, Message} from "discord.js";
import CrawlerService from "./modules/CrawlerService";
import SurveyService from "./modules/SurveyService";


// create a new Discord client
const client = new Discord.Client();
const crawlerService = new CrawlerService();
crawlerService.start();

setInterval(() => {
    crawlerService.stop();
    crawlerService.start();
}, 1000 * 60 * 60);

new SurveyService({apiUrl: process.env.API_URL || '', fetchEndpoint: process.env.PRISMIC_ENDPOINT || ''});

try {
    // when the client is ready, run this code
    // this event will only trigger one time after logging in
    client.once("ready", async () => {
        // @ts-ignore
        await client.user.setStatus("Online");
        console.log("I'm back bitches");
    });

    //listen on event message
    // @ts-ignore
    client.on("message", message => {
        if (message.channel.type === "dm") {
            if (message.author.bot) return;
            dmProcedure(message);
            return;
        }
    });

    // login to Discord with your app's token
    client.login(process.env.DISCORD_TOKEN);
} catch (e) {
    console.log(e);
}

/**
 * In this area we define what happens with direct messages
 * @param message
 */
const dmProcedure = async (message: Message) => {
    const msg = message.content.toLowerCase();
    const commands: string[] = msg.split(" ");

    //The guild object is a discord server
    let guild: Guild;
    // @ts-ignore
    client.guilds.cache.map(g => {
        guild = g;
    });

    //Here we try fetching the author from a common discord server
    // @ts-ignore
    const member: GuildMember = await guild.members.fetch(message.author.id);

    //If we don't share a common server, the author doesn't get his info
    if (!member) {
        message.channel.send("You are not part of the right channel!");
        return;
    }

    //Standard answer
    if (msg === "ping") {
        message.channel.send("pong");
        return;
    }

    //A small help section to navigate through the options
    if (msg.toLowerCase() === "help") {
        message.channel.send(`
    Help Section:\n
    :point_right: !crawler !report shows how many articles were crawled
    :point_right: !crawler !download delivers a .zip archive with all necessary data inside
    `);

        return;
    }

    //What happens on main command !crawler
    if (commands[0].toLowerCase() === "!crawler") {
        // @ts-ignore
        const commandService = new CommandService(commands, crawlerService, message);
        return;
    }

    //If the bot doesn't know what to do
    message.channel.send('Try typing "help" :question:');
    return;
};
